package metrics

import "time"

type noop struct {
}

func (n *noop) Gauge(name string, value int) error {
	return nil
}

func (n *noop) Increment(name string) error {
	return nil
}

func (n *noop) IncrementBy(name string, value int) error {
	return nil
}

func (n *noop) Decrement(name string) error {
	return nil
}

func (n *noop) DecrementBy(name string, value int) error {
	return nil
}

func (n *noop) Duration(name string, duration time.Duration) error {
	return nil
}

func (n *noop) Histogram(name string, value int) error {
	return nil
}

func (n *noop) Annotate(name string, fmt string, args ...interface{}) error {
	return nil
}

func (n *noop) Flush() error {
	return nil
}
