package metrics

import (
	statsd "github.com/statsd/client"
	statsdiface "github.com/statsd/client-interface"
	namespace "github.com/statsd/client-namespace"
	"time"
)

type connection struct {
	client statsdiface.Client
}

func newConnection(addr string) (Metrics, error) {
	c, err := statsd.Dial(addr)
	if err != nil {
		return nil, err
	}
	return &connection{
		client: c,
	}, nil
}

func newConnectionWithNamespace(addr string, ns string) (Metrics, error) {
	c, err := statsd.Dial(addr)
	if err != nil {
		return nil, err
	}
	nsc := namespace.New(c, ns)
	return &connection{
		client: nsc,
	}, nil
}

func (c *connection) Gauge(name string, value int) error {
	return c.client.Gauge(name, value)
}

func (c *connection) Increment(name string) error {
	return c.client.Incr(name)
}

func (c *connection) IncrementBy(name string, value int) error {
	return c.client.IncrBy(name, value)
}

func (c *connection) Decrement(name string) error {
	return c.client.Decr(name)
}

func (c *connection) DecrementBy(name string, value int) error {
	return c.client.DecrBy(name, value)
}

func (c *connection) Duration(name string, duration time.Duration) error {
	return c.client.Duration(name, duration)
}

func (c *connection) Histogram(name string, value int) error {
	return c.client.Histogram(name, value)
}

func (c *connection) Annotate(name string, fmt string, args ...interface{}) error {
	return c.client.Annotate(name, fmt, args)
}

func (c *connection) Flush() error {
	return c.client.Flush()
}
