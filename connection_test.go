package metrics

import (
	"bytes"
	statsd "github.com/statsd/client"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestActualClient(t *testing.T) {
	var w bytes.Buffer
	c := &connection{
		client: statsd.NewClient(&w),
	}
	assert.Nil(t, c.Decrement("bar"))
	assert.Equal(t, "", w.String())

	assert.Nil(t, c.Flush())
	assert.Equal(t, "bar:-1|c", w.String())
}
