package metrics

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNoopTestFunc(t *testing.T) {
	noop := &noop{}
	assert.Nil(t, noop.Increment("foo"))
}

func TestDefaultInitializationNoop(t *testing.T) {
	assert.Nil(t, conn.Decrement("bar"))
}
