package metrics

import (
	"fmt"
	"sync"
	"time"
)

var conn Metrics = &noop{}
var is_noop bool = true

var lock sync.Mutex

// Initialize the global state of this metrics package for use.
// Not calling this will result in a noop-package
func Initialize(host string, port int) error {
	lock.Lock()
	defer lock.Unlock()
	c, err := newConnection(fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		return err
	}
	conn = c
	is_noop = false
	return nil
}

// Initialize the global state of this metrics package for use.
// Not calling this will result in a noop-package
// A prefix is also provided here. Every pushed metric will be prepended by this value.
func InitializeWithPrefix(prefix string, host string, port int) error {
	lock.Lock()
	defer lock.Unlock()
	c, err := newConnectionWithNamespace(fmt.Sprintf("%s:%d", host, port), prefix)
	if err != nil {
		return err
	}
	conn = c
	is_noop = false
	return nil
}

// Check whether this library has been configured during the current execution scope
func IsConfigured() bool {
	return !is_noop
}

// Record an arbitrary value at a given counter
func Gauge(name string, value int) error {
	return conn.Gauge(name, value)
}

// Increment a given counter
func Increment(name string) error {
	return conn.Increment(name)
}

// Increment a given counter by a certain value
func IncrementBy(name string, value int) error {
	return conn.IncrementBy(name, value)
}

// Decrement a given counter
func Decrement(name string) error {
	return conn.Decrement(name)
}

// Decrement a given counter by a certain value
func DecrementBy(name string, value int) error {
	return conn.DecrementBy(name, value)
}

// Record a duration of an event
func Duration(name string, duration time.Duration) error {
	return conn.Duration(name, duration)
}

// Record histograms over time
func Histogram(name string, value int) error {
	return conn.Histogram(name, value)
}

// Create an annotation of a certain name (e.g. deployment, startup, ...)
func Annotate(name string, fmt string, args ...interface{}) error {
	return conn.Annotate(name, fmt, args)
}

// Manually flush outstanding metrics. This will also be done automatically
func Flush() error {
	return conn.Flush()
}
