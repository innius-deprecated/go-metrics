package metrics

import "time"

type Metrics interface {
	// Record an arbitrary value at a given counter
	Gauge(name string, value int) error

	// Increment a given counter
	Increment(name string) error
	// Increment a given counter by a certain value
	IncrementBy(name string, count int) error

	// Decrement a given counter
	Decrement(name string) error
	// Decrement a given counter by a certain value
	DecrementBy(name string, count int) error

	// Record a duration of an event
	Duration(name string, duration time.Duration) error

	// Record histograms over time
	Histogram(name string, value int) error

	// Create an annotation of a certain name (e.g. deployment, startup, ...)
	Annotate(name string, fmt string, args ...interface{}) error

	// Manually flush outstanding metrics. This will also be done automatically
	Flush() error
}
