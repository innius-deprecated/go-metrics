package metrics

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIsConfigured(t *testing.T) {
	assert.False(t, IsConfigured())
}
